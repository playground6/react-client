import React from "react";

class Form extends React.Component {
  
  // state contains all the values that the user typed in the form  
  state = {
    firstName: "",
    lastName: "",
    username: "",
    email: "",
    password: ""
  };

  change = e => {
    //this.props.onChange({ [e.target.name]: e.target.value });
    //this.setState - will update the state i.e value in the text box to the corresponding state
    this.setState({
    //e.target.value - contains what the user typed in the input box
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
    //e.preventDefault will not auto refresh, this line is mandatory  
    e.preventDefault();
    /* the below line will submit the state values to App.js and 
    can be used by the API for database insertion*/
    this.props.onSubmit(this.state);
    //to clear the values of the texbox after submitting the values, the below code is used
    this.setState({
      firstName: "",
      lastName: "",
      username: "",
      email: "",
      password: ""
    })
    /*
    this.props.onChange({
      firstName: "",
      lastName: "",
      username: "",
      email: "",
      password: ""
    }); */
  }

  // render() displays the form on the webpage
  render() {
    return (
      <form>
        <input
          name="firstName"
          placeholder="First name"
          value={this.state.firstName}                          
          //this inputbox calls the default onChange function
          /* The onchange event (e) occurs when the value of an element has 
             been changed. i.e when the user types value in the form */
          //change(e) -> will call the above change function
          onChange={e => this.change(e)} />
        <br />
        <input
          name="lastName"
          placeholder="Last name"
          value={this.state.lastName}
          onChange={e => this.change(e)}
        />
        <br />
        <input
          name="username"
          placeholder="Username"
          value={this.state.username}
          onChange={e => this.change(e)}
        />
        <br />
        <input
          name="email"
          placeholder="Email"
          value={this.state.email}
          onChange={e => this.change(e)}
        />
        <br />
        <input
          name="password"
          type="password"
          placeholder="Password"
          value={this.state.password}
          onChange={e => this.change(e)}
        />
        <br />
        <button onClick={e => this.onSubmit(e)}>Submit</button>
      </form>
    );
  }
}

export default Form