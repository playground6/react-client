// MyComponent.js
import React, { Component } from 'react';

class App extends Component {
  render() {
    return (
      <div>This is my component.</div>
    );
  }
}

export default App;
