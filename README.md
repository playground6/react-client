# This app is created using

npx create-react-app react-client

# Running the application

1. docker login registry.gitlab.com
2. docker build -t registry.gitlab.com/playground6/react-client .
3. docker push registry.gitlab.com/playground6/react-client
4. docker run -d -p 80:80 registry.gitlab.com/playground6/react-client

